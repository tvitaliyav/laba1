import pandas as pd
import numpy as np


def del_user(data):
    array = data.copy()
    for i in range(0, len(array)):
        r = array['user'][i]
    s = ''.join(x for x in r if x.isdigit())
    array['user'][i] = int(s)
    return array


def to_int(string):
    return list(map(int, string.split(';')))


def get_matrix(dim, data):
    length = len(data)
    matrix = np.zeros([dim, dim])

    if length > 1:
        i = 0
        while i < length - 1:
            matrix[data[i]][data[i + 1]] += 1
            i += 1

    matrix[matrix == 0] += 10e-6

    i = 0
    while i < dim:
        matrix[i] = matrix[i] / matrix[i].sum()
        i += 1

    return matrix


def check(matrix):
    i = 0
    condition = True
    while i < len(matrix) and condition:
        if float(matrix[i].sum()) < 0.999999999:
            condition = False

        i += 1

    if not condition:
        print('Matrix is corrupted.')
    else:
        print('Matrix is correct.')

    return


def probability(values, matrix, window):
    length = len(values)
    minimum = 1

    i = 0

    while i < length - (window + 1):
        j = 0
        prob = 1

        while j < window:
            prob *= matrix[values[i + j]][values[i + j + 1]]
            j += 1

        if prob < minimum:
            minimum = prob

        i += 1

    return minimum


def detect_anomalies(val, matrix, probability, window):
    array = val
    length = len(array)
    if length > 1:
        i = 0
        while i < length - (window + 1):
            j = 0
            prob = 1
            while j < window:
                prob *= matrix[array[i + j]][array[i + j + 1]]
                j += 1

            if prob < probability:
                return 1

            i += 1

    return 0


# read files
data = pd.read_csv('data.txt', header=None, sep=':')
fake = pd.read_csv('data_fake.txt', header=None, sep=':')
true = pd.read_csv('data_true.txt', header=None, sep=':')

# rename columns
data.columns = ['user', 'data']
fake.columns = ['user', 'data']
true.columns = ['user', 'data']

# sort values
data = data.sort_values(['user'], ignore_index=True)
fake = fake.sort_values(['user'], ignore_index=True)
true = true.sort_values(['user'], ignore_index=True)

print(data)
print(fake)
print(true)

Max = []
Min = []
for i in range(0, len(data)):
    list_ = to_int(data['data'][i])
    Max.append(max(list_))
    Min.append(min(list_))

max = max(Max)
min = min(Min)

print('max value = ' + str(max))
print('min value = ' + str(min))

dim = max - min + 1
window = 5

data['TRUE'] = 0
data['FAKE'] = 0
for i in range(0, len(data)):
    d = to_int(data['data'][i])
    f = to_int(fake['data'][i])
    t = to_int(true['data'][i])

    matrix = get_matrix(dim, d)
    check(matrix)

    print(i)
    print(matrix)

    threshold = probability(d, matrix, window)

    print('threshold = ' + str(threshold))

    res_t = detect_anomalies(t, matrix, threshold, window)
    res_f = detect_anomalies(f, matrix, threshold, window)

    data['TRUE'].loc[i] = res_t
    data['FAKE'].loc[i] = res_f

print(data)
print('Процент ложных срабатываний: ', data[data['TRUE'] == 1].shape[0] / data.shape[0] * 100)
print('Процент пропущенных: ', data[data['FAKE'] == 0].shape[0] / data.shape[0] * 100)
